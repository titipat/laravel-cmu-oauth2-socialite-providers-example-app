<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('redirect', function () {
    return Socialite::driver('cmu')->redirect();
});

Route::get('callback', function () {
    $cmuUser = Socialite::driver('cmu')->user();

    $user = User::updateOrCreate([
        'id' => $cmuUser->getId(),
    ], [
        'email' => $cmuUser->getEmail(),
        'firstname_en' => Str::ucfirst(Str::lower($cmuUser->firstname_EN)),
        'lastname_en' => Str::ucfirst(Str::lower($cmuUser->lastname_EN)),
        'firstname_th' => $cmuUser->firstname_TH,
        'lastname_th' => $cmuUser->lastname_TH,
    ]);

    Auth::login($user);

    return response()->json($cmuUser);
    // {
    //     "id": "********************************",
    //     "nickname": null,
    //     "name": "********************************",
    //     "email": "********************************@cmu.ac.th",
    //     "avatar": null,
    //     "user": {
    //       "cmuitaccount_name": "********************************",
    //       "cmuitaccount": "********************************@cmu.ac.th",
    //       "student_id": "********************************",
    //       "prename_id": "MR",
    //       "prename_TH": "นาย",
    //       "prename_EN": "Mr.",
    //       "firstname_TH": "********************************",
    //       "firstname_EN": "********************************",
    //       "lastname_TH": "********************************",
    //       "lastname_EN": "********************************",
    //       "organization_code": "06",
    //       "organization_name_TH": "คณะวิศวกรรมศาสตร์",
    //       "organization_name_EN": "Faculty of Engineering",
    //       "itaccounttype_id": "AlumAcc",
    //       "itaccounttype_TH": "นักศึกษาเก่า",
    //       "itaccounttype_EN": "Alumni Account"
    //     },
    //     "attributes": {
    //       "id": "********************************",
    //       "email": "********************************@cmu.ac.th",
    //       "name": "********************************",
    //       "student_id": "********************************",
    //       "prename_id": "MR",
    //       "prename_TH": "นาย",
    //       "prename_EN": "Mr.",
    //       "firstname_TH": "********************************",
    //       "firstname_EN": "********************************",
    //       "lastname_TH": "********************************",
    //       "lastname_EN": "********************************",
    //       "organization_code": "06",
    //       "organization_name_TH": "คณะวิศวกรรมศาสตร์",
    //       "organization_name_EN": "Faculty of Engineering",
    //       "itaccounttype_id": "AlumAcc",
    //       "itaccounttype_TH": "นักศึกษาเก่า",
    //       "itaccounttype_EN": "Alumni Account"
    //     },
    //     "token": "********************************",
    //     "refreshToken": "********************************",
    //     "expiresIn": 3600,
    //     "approvedScopes": null,
    //     "accessTokenResponseBody": {
    //       "access_token": "********************************",
    //       "expires_in": 3600,
    //       "refresh_token": "********************************"
    //     }
    //   }
});
