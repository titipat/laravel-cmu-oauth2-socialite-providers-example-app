<?php

namespace SocialiteProviders\CMU;

use SocialiteProviders\Manager\SocialiteWasCalled;

class CMUExtendSocialite
{
    /**
     * Register the provider.
     *
     * @param \SocialiteProviders\Manager\SocialiteWasCalled $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('cmu', Provider::class);
    }
}
