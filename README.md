# Laravel CMU OAuth2 Socialite Providers Example App

Quick steps

1. Install Socialite

```sh
composer require laravel/socialite
```

2. Install Socialite generator

```sh
composer require socialiteproviders/generators
```

3. Add generator's service to app's provider in `config/app.php`

```php
// config/app.php
'providers' => [
    SocialiteProviders\Generators\GeneratorsServiceProvider::class,
],
```

4. Generate provider

```sh
php artisan make:socialite CMU --spec=oauth2 --author=YourName --email=your@name.com --scopes=cmuitaccount.basicinfo --authorize_url=https://oauth.cmu.ac.th/v1/Authorize.aspx --access_token_url=https://oauth.cmu.ac.th/v1/GetToken.aspx --user_details_url=https://misapi.cmu.ac.th/cmuitaccount/v1/api/cmuitaccount/basicinfo
```

5. Run composer dump autoload

```sh
composer dumpautoload
```

6. Install Socialite providers manager

```sh
composer require socialiteproviders/manager
```

7. Add created provider to the app's event provider in `app/Providers/EventServiceProvider.php`

```php
// app/Providers/EventServiceProvider.php
protected $listen = [
    \SocialiteProviders\Manager\SocialiteWasCalled::class => [
        'SocialiteProviders\\CMU\\CMUExtendSocialite@handle'
    ],
];
```

8. Change `mapUserToObject()` in `SocialiteProviders/src/CMU/Provider.php` as the following

```php
// SocialiteProviders/src/CMU/Provider.php
protected function mapUserToObject(array $user)
{
    return (new User())->setRaw($user)->map([
        'id' => $user['cmuitaccount_name'],
        'email' => $user['cmuitaccount'],
        'name' => $user['firstname_EN'] . ' ' . $user['lastname_EN'],
        'student_id' => $user['student_id'],
        'prename_id' => $user['prename_id'],
        'prename_TH' => $user['prename_TH'],
        'prename_EN' => $user['prename_EN'],
        'firstname_TH' => $user['firstname_TH'],
        'firstname_EN' => $user['firstname_EN'],
        'lastname_TH' => $user['lastname_TH'],
        'lastname_EN' => $user['lastname_EN'],
        'organization_code' => $user['organization_code'],
        'organization_name_TH' => $user['organization_name_TH'],
        'organization_name_EN' => $user['organization_name_EN'],
        'itaccounttype_id' => $user['itaccounttype_id'],
        'itaccounttype_TH' => $user['itaccounttype_TH'],
        'itaccounttype_EN' => $user['itaccounttype_EN'],
    ]);
}
```

9. Add your id, secret, and redirect uri in `.env`

```env
OAUTH_APP_ID="********************************"
OAUTH_APP_SECRET="********************************"
OAUTH_REDIRECT_URI="https://********************************.cmu.ac.th/callback"
```

10. Implement redirect and callback endpoints

```php
// routes/web.php
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

Route::get('redirect', function () {
    return Socialite::driver('cmu')->redirect();
});

Route::get('callback', function () {
    $cmuUser = Socialite::driver('cmu')->user();

    $user = User::updateOrCreate([
        'id' => $cmuUser->getId(),
    ], [
        'email' => $cmuUser->getEmail(),
        'firstname_en' => Str::ucfirst(Str::lower($cmuUser->firstname_EN)),
        'lastname_en' => Str::ucfirst(Str::lower($cmuUser->lastname_EN)),
        'firstname_th' => $cmuUser->firstname_TH,
        'lastname_th' => $cmuUser->lastname_TH,
    ]);

    Auth::login($user);

    return response()->json($cmuUser);
    // {
    //     "id": "********************************",
    //     "nickname": null,
    //     "name": "********************************",
    //     "email": "********************************@cmu.ac.th",
    //     "avatar": null,
    //     "user": {
    //       "cmuitaccount_name": "********************************",
    //       "cmuitaccount": "********************************@cmu.ac.th",
    //       "student_id": "********************************",
    //       "prename_id": "MR",
    //       "prename_TH": "นาย",
    //       "prename_EN": "Mr.",
    //       "firstname_TH": "********************************",
    //       "firstname_EN": "********************************",
    //       "lastname_TH": "********************************",
    //       "lastname_EN": "********************************",
    //       "organization_code": "06",
    //       "organization_name_TH": "คณะวิศวกรรมศาสตร์",
    //       "organization_name_EN": "Faculty of Engineering",
    //       "itaccounttype_id": "AlumAcc",
    //       "itaccounttype_TH": "นักศึกษาเก่า",
    //       "itaccounttype_EN": "Alumni Account"
    //     },
    //     "attributes": {
    //       "id": "********************************",
    //       "email": "********************************@cmu.ac.th",
    //       "name": "********************************",
    //       "student_id": "********************************",
    //       "prename_id": "MR",
    //       "prename_TH": "นาย",
    //       "prename_EN": "Mr.",
    //       "firstname_TH": "********************************",
    //       "firstname_EN": "********************************",
    //       "lastname_TH": "********************************",
    //       "lastname_EN": "********************************",
    //       "organization_code": "06",
    //       "organization_name_TH": "คณะวิศวกรรมศาสตร์",
    //       "organization_name_EN": "Faculty of Engineering",
    //       "itaccounttype_id": "AlumAcc",
    //       "itaccounttype_TH": "นักศึกษาเก่า",
    //       "itaccounttype_EN": "Alumni Account"
    //     },
    //     "token": "********************************",
    //     "refreshToken": "********************************",
    //     "expiresIn": 3600,
    //     "approvedScopes": null,
    //     "accessTokenResponseBody": {
    //       "access_token": "********************************",
    //       "expires_in": 3600,
    //       "refresh_token": "********************************"
    //     }
    //   }
});
```

## References

-   https://laravel.com/docs/10.x/socialite#main-content
-   https://github.com/SocialiteProviders/Generators
-   https://github.com/SocialiteProviders/Manager
